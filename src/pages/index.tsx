import { Container } from "@/ui/components/container/Container";
import { Navigation } from "@/ui/components/navigation/Navigation";
import { Seo } from "@/ui/components/seo/seo";
import { Avatar } from "@/ui/design-system/avatar/Avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/Logo";
import { Spinner } from "@/ui/design-system/spinner/Spinner";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { RiAlertFill, RiUserFill, RiArrowRightFill } from "react-icons/ri";

export default function Home() {
  return (
    <Container className="my-20 flex items-center gap-7">
      <div className="">
        <Typography variant="h1" component="h1" theme="primary">
          Vous voulez créer un site ?
        </Typography>
        <Typography variant="lead" component="p" theme="gray">
          Pas de panique, vous êtes au bon endroit ! Notre équipe de passionnée saura concrétisé tous vos besoins !
        </Typography>
        <div className="flex items-center gap-2 my-5">
          <Button variant="accent" size="small">
            Les tarifs
          </Button>
          <Button variant="secondary" size="small">
            En savoir plus
          </Button>
        </div>
      </div>
      <div>
        <Image
          className=""
          src="../../../assets/images/illustration.png"
          alt="Fusée qui va décollée"
          height={1000}
          width={1000}
        />
      </div>
    </Container>
  );
}
