import { Navigation } from "@/ui/components/navigation/Navigation";
import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body>
        <Navigation/>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
