import { Container } from "@/ui/components/container/Container";
import { Navigation } from "@/ui/components/navigation/Navigation";
import { Avatar } from "@/ui/design-system/avatar/Avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/Logo";
import { Spinner } from "@/ui/design-system/spinner/Spinner";
import { Typography } from "@/ui/design-system/typography/typography";
import { RiAlertFill, RiArrowRightFill, RiUserFill } from "react-icons/ri";

export default function DesignSystem(){
  return (
    <>
      <Navigation />
      <Container className="space-y-10 py-10">
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Avatar
            </Typography>
            <div className="flex items-center gap-x-5 p-5 border border-gray-400 rounded">
              <Avatar size="small" src="..\..\..\assets\images\daniel-lincoln.jpg" alt="Lorem"/>
              <Avatar src="..\..\..\assets\images\daniel-lincoln.jpg" alt="Lorem"/>
              <Avatar size="large" src="..\..\..\assets\images\systeme-html-pour-concept-site-web.jpg" alt="Lorem"/>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Logos
            </Typography>
            <div className="flex items-center gap-x-5 p-5 border border-gray-400 rounded">
              <Logo size="very-small"/>
              <Logo size="small"/>
              <Logo/>
              <Logo size="large"/>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Bouton classique
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Button isLoading={false} variant="accent" size="small">Accent</Button>
              <Button variant="secondary" size="small">Secondary</Button>
              <Button variant="outline" size="small">Outline</Button>
              <Button variant="disabled" size="small">Disabled</Button>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Bouton loading
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Button isLoading variant="accent" size="medium">Accent</Button>
              <Button isLoading variant="secondary" size="medium">Secondary</Button>
              <Button isLoading variant="outline" size="medium">Outline</Button>
              <Button isLoading variant="disabled" size="medium">Disabled</Button>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Big button
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Button variant="accent" size="large">Accent</Button>
              <Button variant="secondary" size="large">Secondary</Button>
              <Button variant="outline" size="large">Outline</Button>
              <Button variant="disabled" size="large">Disabled</Button>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Icon button
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Button icon={{icon: RiAlertFill}} variant="ico" />
              <Button icon={{icon: RiAlertFill}} variant="ico" iconTheme="secondary"/>
              <Button icon={{icon: RiUserFill}} variant="ico" iconTheme="gray"/>
              <Button variant="accent" icon={{icon: RiAlertFill}} iconPosition="left"  size="large">Accent</Button>
              <Button variant="outline" icon={{icon: RiArrowRightFill}} iconPosition="left"  size="large">Accent</Button>
            </div>
          </div>
        </div>
        <div className="flex items-start gap-7">
          <div className="space-y-2">
            <Typography variant="h2">
              Spinner
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
              <Spinner size="small"/>
              <Spinner />
              <Spinner size="large"/>
            </div>
          </div>
        </div>






          {/* <Typography variant="display" component="p" weight="medium" theme="primary">
            Nothing <span className="text-indigo-500">is</span> impossible
          </Typography>
          <Typography variant="h1" component="p" theme="secondary">
            Nothing is impossible, the word itself says, I’m possible!
          </Typography>
          <Typography variant="h2" component="p" theme="gray">
            Your time is limited, so don’t waste it living someone else’s life
          </Typography>
          <Typography variant="h3" component="p" theme="black" >
            Daily Report: Removing Checks to the Power of the Internet Titans
          </Typography>
          <Typography variant="h4" component="p" theme="white" className="" >
            Daily Report: Removing Checks to the Power of the Internet Titans
          </Typography>
          <Typography variant="h5" component="p" >
            Daily Report: Removing Checks to the Power of the Internet Titans
          </Typography>
          <Typography variant="lead" component="p" >
            Nothing is impossible, the word itself says, I’m possible!
          </Typography>
          <Typography variant="body-base" component="p" >
            Nothing is impossible, the word itself says, I’m possible!
          </Typography> */}

      </Container>
    </>
  )
}