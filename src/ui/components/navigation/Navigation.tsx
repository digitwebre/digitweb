import { Logo } from "@/ui/design-system/logo/Logo"
import { Container } from "../container/Container"
import { Typography } from "@/ui/design-system/typography/typography"
import { Button } from "@/ui/design-system/button/button"

interface Props{}

export const Navigation = ({}: Props) => {
  return (
    <div className="border-b-2 border-gray-400">
      <Container className="flex justify-between items-center gap-7 py-1.5">
        <div className="flex items-center gap-2.5 ">
          <Logo />
          <div className="flex flex-col ">
            <div className="text-gray font-extrabold text-[24px]">
              Digitweb Réunion
            </div>
            <Typography variant="caption4" theme="gray" weight="regular" component="span">
              Trouve de l’inspiration & reçois des feedbacks !
            </Typography>
          </div>
        </div>
        <div className="flex items-center gap-7">
          <Typography variant="caption3" theme="primary" component="div" className="flex items-center gap-7">
            <span>Projet</span>
            <span>Formations</span>
            <span>Contact</span>
          </Typography>
          <div className="flex items-center gap-2">
            <Button size="small" variant="secondary" iconTheme="secondary" >
              Connexion
            </Button>
            <Button size="small" variant="accent" iconTheme="secondary" >
              Rejoindre
            </Button>
          </div>
        </div>
      </Container>
    </div>
  )
}